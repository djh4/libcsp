.. CSP Client master file

.. |cps-client| replace:: CSP Client

######################################
Welcome to CSP Client Documentation
######################################

.. only:: html

   Release |release| - |today|

.. toctree::
    :maxdepth: 2
   
    /doc/csp-client

.. * :ref:`genindex`
.. * :ref:`search`

